<h1><?= __("Praktiline ülesanne: ") ?> <?= $practical['practical_name'] ?></h1>
<table class="table table-bordered">

    <tr>
        <th><?= __("Ülesande") ?> ID</th>
        <td><?= $practical['practical_id'] ?></td>
    </tr>

    <tr>
        <th><?= __("Ülesande kirjeldus") ?></th>
        <td><?= $practical['practical_description'] ?></td>
    </tr>

    <tr>
        <th><?= __("Ülesande nimi") ?></th>
        <td><?= $practical['practical_name'] ?></td>
    </tr>


</table>

<!-- EDIT BUTTON -->
<?php if ($auth->is_admin): ?>
    <form action="practicals/edit/<?= $practical['practical_id'] ?>">
        <div class="pull-right">
            <button class="btn btn-primary">
                <?= __("Edit") ?>
            </button>
        </div>
    </form>
<?php endif; ?>