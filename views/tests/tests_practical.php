<div align="center">
    <div style="background-color: #eaeaea; width: 614px; padding: 5px;margin: 5px; border-radius: 10px; border: 1px solid black;" ">
    <h3>Praktiline ülesanne</h3>
    <?= nl2br($practical['practical_description'],true) ?></div>

<?php $_SESSION['practical_id'] = $practical['practical_id']?>

<form method="post" id="form">
    <textarea id="codeeditor" name="data" data-editor="xml" rows=30 style="width: 620px; height: 300px;"></textarea><br>
    <button class="btn btn-primary" type="submit"><?= __("Esita") ?></button>
</form>
</div>

<script src="assets/ace-builds-master/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/ace-builds-master/src-noconflict/theme-twilight.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/ace-builds-master/src-noconflict/mode-html.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="assets/ace-builds-master/src-noconflict/style.css">
<script src="assets/js/jquery.js" type="text/javascript" charset="utf-8"></script>


<script>
    // Hook up ACE editor to all textareas with data-editor attribute
    $(function () {
        $('textarea[data-editor]').each(function () {
            var textarea = $(this);
            var editDiv = $('<div>', {
                position: 'absolute',
                width: textarea.width(),
                height: textarea.height(),
                'class': textarea.attr('class')
            }).insertBefore(textarea);
            textarea.css('display', 'none');
            var editor = ace.edit(editDiv[0]);
            editor.renderer.setShowGutter(false);
            editor.setTheme("ace/theme/monokai"); // Theme->monokai
            editor.getSession().setMode("ace/mode/html"); // Mode->htl
            editor.getSession().setUseWrapMode(true);//Läheb mitmele reale tekst
            // editor.setTheme("ace/theme/idle_fingers");

            // copy back to textarea on form submit...
            textarea.closest('form').submit(function () {
                textarea.val(editor.getSession().getValue());
            })
        });
    });
</script>