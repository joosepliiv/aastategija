<h1><?= __("Question") ?> '<?= $question['question_name'] ?>'</h1>
<table class="table table-bordered">

    <tr>
        <th><?= __("Question") ?> <?= __("ID") ?></th>
        <td colspan="2"><?= $question['question_id'] ?></td>
    </tr>

    <tr>
        <th><?= __("Question") ?> <?= __("name") ?></th>
        <td colspan="2"><?= $question['question_name'] ?></td>
    </tr>


    <?php for ($i=0; $i < 3; $i++){ ?>
        <tr>
            <th><?= __("Answer") ?> <?= $i+1 ?> <?= __("name") ?></th>
            <td><?= $answers[$i]['answer'] ?></td>
            <td><label>Is right?: </label><?= $answers[$i]['right_answer'] ?></td>

        </tr>
    <?php } ?>
</table>

<!-- EDIT BUTTON -->
<?php if ($auth->is_admin): ?>
    <form action="questions/edit/<?= $question['question_id'] ?>">
        <div class="pull-right">
            <button class="btn btn-primary">
                <?= __("Edit") ?>
            </button>
        </div>
    </form>
<?php endif; ?>