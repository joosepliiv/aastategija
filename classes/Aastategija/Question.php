<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 3/23/2017
 * Time: 12:17 PM
 */

namespace Aastategija;


class Question
{

    static function edit_question($data,$answer_data,$subcheck){

        //convert question name so you can use tags in question
        $data['question_name'] = htmlentities($data['question_name'], ENT_QUOTES);

        //update question name in questions table
        update('questions', $data,"question_id = '$data[question_id]'");

        //Delete old answers and get those new into DB

        //if $subcheck is 1, answer is right
        (isset($subcheck)) ? 1 : 0;

        //delete old answers
        q("DELETE FROM answers WHERE question_id ='$data[question_id]'");

        //look for all answers
        for($i = 1; $i<4; $i++){

            //convert answer so you can use tags in them
            $answer = htmlentities($answer_data["answer_$i"], ENT_QUOTES);

            //control if the answer is right
            $right_answer = (isset($answer_data["right_answer_$i"])) ? 1 : 0;

            //insert answer into answers table
            $insert_data = array('answer' => $answer, 'right_answer' => $right_answer, 'question_id' => $data['question_id']);
            insert('answers',$insert_data);

        }

        // Locate user to questions index page
        header('Location: ' . BASE_URL . 'questions/');
    }

    static function add_question($data,$answer_data){

        //Convert question name so you can use tags in them and insert them to questions table
        $data['question_name'] = htmlentities($data['question_name'], ENT_QUOTES);
        insert("questions",$data);

        //Get question_id that has been generated
        $question_id = get_one("SELECT question_id FROM questions WHERE question_name = '$data[question_name]'");

        //Add answers
        for($i = 1; $i<4; $i++){

            //Convert answer so you can use tags in them
            $answer = htmlentities($answer_data["answer_$i"], ENT_QUOTES);

            //check if the answer is right
            $right_answer = (isset($answer_data["right_answer_$i"])) ? 1 : 0;

            //Insert answer into answer table
            $insert_data = array('answer' => $answer, 'right_answer' => $right_answer, 'question_id' => $question_id);
            insert('answers',$insert_data);

        }

        header('Location: ' . BASE_URL . 'questions/');
    }
}