<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 3/23/2017
 * Time: 12:37 PM
 */

namespace Aastategija;


class Practicals
{

    static function add_prac($data)
    {
        //Convert answer so you can use tags in them
        $data['answer'] = htmlentities($data['answer'], ENT_QUOTES);

        //insert into table practical
        insert('practicals', $data);
    }

    static function edit_prac($practical_id,$data)
    {
        //convert answer to use tags in answer
        $data['answer'] = htmlentities($data['answer'], ENT_QUOTES);

        //update practicals table
        update('practicals', $data,"practical_id= '$practical_id'");

        header('Location: ' . BASE_URL . 'practicals/');
    }


    static function delete_prac($practical_id){
        return q("DELETE FROM practicals WHERE practical_id = '{$practical_id}'") ? 'Ok' : 'Fail';
        header('Location: ' . BASE_URL . 'practicals/');
    }
}