<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 3/23/2017
 * Time: 12:49 PM
 */

namespace Aastategija;


class Pscores
{
    static function insert_score($user_id,$score){

        //Get theory question score
        $question_score = get_one("SELECT Questions_score FROM users WHERE user_id = '{$user_id}'");

        //Insert practical test score and mark it as checked in prac_answers table
        $data = array("score" => $score, "checked" => 1);
        update("prac_answers",$data,"user_id = {$user_id}");

        //Insert practical test score, and overall score into user table
        $data = array("Practical_checked" => 1, "prac_score" => $score, "score" => $score+$question_score);
        update("users",$data,"user_id = {$user_id}");

        //locate user to pscores index
        header('Location: ' . BASE_URL . 'pscores/');
    }


    static function reset_user($user_id){

        //Reset user tests data
        $data = array("test_done" => 0, "prac_done" => 0, "Practical_checked" => 0, "Questions_score" => 0, "prac_score" => 0, "score" => 0);
        update("users",$data,"user_id = {$user_id}");

        //Delete users old answers
        q("DELETE FROM prac_answers WHERE user_id = {$user_id}");

        return "ok";
    }

}