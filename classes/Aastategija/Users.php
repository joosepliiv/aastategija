<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 3/23/2017
 * Time: 9:17 AM
 */

namespace Aastategija;

class Users
{

    //add user
    static function new_user($data)
    {
        //Change password to password_hash
        $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
        insert('users', $data);
    }


    //Change user info
    static function edit_user($data)
    {
        //if password is inserted convert it with password_hash, else unset password
        if ($data['password'] !== ""){
            $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
        }
        else{
            unset($data['password']);
        }
        insert('users', $data);
        header('Location: ' . BASE_URL . 'users/');
    }


    //change "deleted" to 1 in user table
    static function delete_user($user_id){
        update('users', ['deleted' => '1'], "user_id = '$user_id'");
    }
}

?>