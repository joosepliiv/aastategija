<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 3/23/2017
 * Time: 10:37 AM
 */

namespace Aastategija;

class Tests
{
    static function check_answers($data,$user_id){

        $score = 0;

        foreach ($data as $question_id => $answer){

            //Find right answer from database
            $right_answer = get_one("SELECT answer FROM answers WHERE question_id = '{$question_id}' AND right_answer = 1");

            if ($answer == html_entity_decode($right_answer,ENT_QUOTES)){
                //answer is right
                //increase score
                $score++;
            }
            else{
                $score = $score;
            }
        }
        $data = array("Questions_score" => $score, "test_done" => 1);

        //Update user score in db
        update('users', $data,"user_id = '$user_id'");
        //refresh page
        header("Refresh:0");
    }

    static function insert_prac($answer,$user_id,$question_id){

        //set up data to insert into prac_answers
        $answer = htmlentities($answer, ENT_QUOTES);
        $data = array("answer"=>$answer, "user_id"=>$user_id, "question_id"=>$question_id);

        //Insert answer into prac_answers
        insert("prac_answers",$data);


        //set prac_done => 1 in users table
        $data = array("prac_done"=>1);
        update("users",$data,"user_id = '$_SESSION[user_id]'");

        //Go to /tests/index page
        header('Location: ' . BASE_URL . 'tests/');
    }


    static function shuffle($questions){

        // shuffle all the quesitons to get random questions, and then take only 10 first of array
        shuffle($questions);
        $questions = array_slice($questions,0,10);
        return $questions;

    }
}