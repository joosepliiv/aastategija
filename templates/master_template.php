<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tartu KHK Aastategija 2017 projekt">
    <meta name="author" content="Kaspar Suvi, Joosep Liiv, Anna-Lotta Kaukes">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title><?= PROJECT_NAME ?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/components/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/radio-style.css">

    <!-- Custom styles for this template -->


    <!-- jQuery -->
    <script src="vendor/components/jquery/jquery.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>

<body>
<?php if (isset($auth->is_admin) AND $auth->is_admin): ?>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li <?= $this->controller == 'tests' ? 'class="active"' : ""; ?>><a href="tests"><?= __('Test') ?></a></li>
                <li <?= $this->controller == 'results' ? 'class="active"' : ""; ?>><a href="results"><?= __('Tulemused') ?></a></li>

                <li class="dropdown" <?= $this->controller == 'questions' ? 'class="active"' : ""; ?>>
                    <a class="dropdown-toggle" data-toggle="dropdown" <?= $this->controller == 'questions' ? 'class="active"' : ""; ?>><?= __('Teooria küsimused') ?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="questions"><?= __('Vaata küsimusi') ?></a></li>
                        <li><a href="questions/add"><?= __('Lisa uus') ?></a></li>
                    </ul>
                </li>
                <li <?= $this->controller == 'practicals' ? 'class="active"' : ""; ?>><a href="practicals"><?= __('Praktilised ülesanded') ?></a></li>
                <li <?= $this->controller == 'users' ? 'class="active"' : ""; ?>><a href="users"><?= __('Kasutajad') ?></a></li>
                <li <?= $this->controller == 'pscores' ? 'class="active"' : ""; ?>><a href="pscores"><?= __('Praktilise testi hindamine') ?></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a <?= $auth->logged_in == true ? 'href="logout"' : 'href="login"'; ?>><?= $auth->logged_in == true ? __('Logi välja') : __('Logi sisse'); ?></a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->

    </div>
</div>
<?php endif; ?>
<div id="wrap">
    <div class="container">
<?php if (!$auth->is_admin): ?>
    <div id="header">
        <div id="logo">
            <a href="http://khk.ee/" target="_blank"><img src="views/welcome/KHK_logo_hor.png" alt="logo" height="100px"></a>
        </div>
        <div id="title">
            <h1>Sisseastumise HTML & CSS test</h1>
        </div>
        <div class="pull-right">
            <?= $auth->first_name," ", $auth->last_name ?>
            <form action="logout" style="margin-top:1px;"><button class="btn btn-default">Logi välja</button></form>
        </div>
    </div>
    <hr>
<?php endif; ?>


    <!-- Main component for a primary marketing message or call to action -->
    <?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
    <?php @require "views/$controller/{$controller}_$action.php"; ?>

    </div>
    <br><br>
</div>
<!-- /container -->
<div id="footer">
    <div class="container">
        <div id="contact" class="pull-left">
            <p><strong>Tartu Kutsehariduskeskus</strong>
            <br>
                Kopli 1, 50115 Tartu</p>
        </div>


        <div id="logos" class="pull-right">
            <div>
                <a href="http://www.facebook.com/kutseharidus" target="_blank"><img src="http://khk.ee/wp/wp-content/themes/khk/images/facebook.png" alt="Facebook"></a>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="vendor/components/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
