<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <title><?= PROJECT_NAME ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap-theme.min.css">
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="vendor/components/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

<div class="container">
<div id="header">
    <div id="logo">
        <a href="http://khk.ee/" target="_blank"><img src="views/welcome/KHK_logo_hor.jpg" alt="logo" height="100px"></a>
    </div>
    <div id="title">
        <h1>Sisseastumise HTML & CSS test</h1>
    </div>
</div>
<hr>
    <div id="content">
        <div id="text">
            <h3>Tere tulemast testile! </h3>
            <p>Käesolev test on loodud testimaks Tartu Kutsehariduskeskusesse sisseastujate oskuseid ja teadmiseid HTML ja CSS programmeerimis keeltes. Sisse logimiseks kasuta oma isikukoodi ning parooli, mille eelnevalt testi läbiviijalt said. Test koosneb teoreetilisest ning praktilisest osast. Teoreetilises osas on 10 valikvastustega küsimust HTMLi ja CSSi kohta, õigeid vastuseid on igal küsimusel ainult üks. Praktilises osas on üks töökäsk, mis eeldab HTML/CSS oskust. Peale testi vastuste esitamist ei saa enam tagasi minna, et vastuseid muuta. Kontrolli enne edasi minemist kõik üle!</p>
        </div>
        <form id="form-login" class="form-signin" method="post">

            <?php if (isset($errors)) {
                foreach ($errors as $error): ?>
                    <div class="alert alert-danger">
                        <?= $error ?>
                    </div>
                <?php endforeach;
            } ?>


            <label for="user"><?= __('Isikukood') ?></label>

            <div class="input-group">
                <input id="user" name="email" type="text" class="form-control" maxlength="11" placeholder="isikukood" autofocus>
            </div>

            <br/>

            <label for="pass"><?= __('Parool') ?></label>

            <div class="input-group">
                <input id="pass" name="password" type="password" class="form-control" placeholder="***********">
            </div>

            <br/>

            <button class="btn btn-lg btn-primary btn-block" type="submit"><?= __('Alusta testi!') ?></button>
        </form>
    </div>
</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
